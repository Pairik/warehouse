﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseProgram
{
    class WarehouseBot
    {
        List<Article> articles = new List<Article>();
        public WarehouseBot(List<Article> articles)
        {
            this.articles = articles;
        }
        public bool Move(Article article,string destination)
        {
            bool move = false;
            article.WarehouseLocation = destination;
            move = true;
            return move;
        }
        public bool Delete(Article article)
        {
            articles.Remove(article);
            Console.WriteLine(article.Title + " was deleted!");
            article.DateDelete = DateTime.Now;
            article.Deleted = true;
            return article.Deleted;
        }
        public Article Find(string barcode)
        {
            Article search = articles[0];
            for (int i = 0; i < articles.Count; i++)
            {
                if (articles[i].BarCode.Equals(barcode))
                {
                    search = articles[i];
                    break;
                }
            }
            return search;
        }
        public bool Scrapp(Article article)
        {
            articles.Remove(article);
            Console.WriteLine(article.Title + " went for scrapp!");
            article.DateDelete = DateTime.Now;
            article.Scrapped = true;

            return article.Scrapped;
        }
        public bool Export(Article article)
        {
            bool exported = false;
            articles.Remove(article);
            Console.WriteLine(article.Title + " was exported! Someone bought it.");
            article.DateExport = DateTime.Now;
            exported = true;
            return exported;
        }
        public bool Import(Article article)
        {
            bool add = false;
            articles.Add(article);
            article.DateImport = DateTime.Now;
            add = true;
            return add;
        }
    }
}
