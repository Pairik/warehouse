﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseProgram
{
    class Furniture : Article
    {
        public string Color { get; set; }
        public string CompleteSet { get; set; }
        public string Materials { get; set; }
        public string Warranty { get; set; }
        public string Height { get; set; }
        public string Width { get; set; }
        public string Depth { get; set; }
        public string SlabThickness { get; set; }
        public Furniture(string title, string description, string warehouseLocation, string barCode, string CompleteSet, string Materials, string Warranty, string Height, string Width, string Depth,
            string SlabThickness, string Color, string CountryOfOrigin) : base(title, description, warehouseLocation, barCode)
        {
           
            this.CompleteSet = CompleteSet;
            this.Materials = Materials;
            this.Warranty = Warranty;
            this.Height = Height;
            this.Width = Width;
            this.Depth = Depth;
            this.SlabThickness = SlabThickness;
            this.CountryOfOrigin = CountryOfOrigin;
        }
    }
}
