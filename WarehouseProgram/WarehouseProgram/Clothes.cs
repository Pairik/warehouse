﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseProgram
{
    class Clothes : Article
    {
        public string Fabric { get; set; }
        public string Size { get; set; }
        public string Gender { get; set; }
        public Clothes(string title, string description, string warehouseLocation, string barCode, string Fabric, string Gender, string Size) : base(title, description, warehouseLocation,barCode)
        {
           
            this.Fabric = Fabric;
            this.Gender = Gender;
            this.Size = Size;
        }
    }
}
