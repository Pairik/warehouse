﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseProgram
{
    class Technics : Article
    {
        public string Manufacturer { get; set; }
        public string Function{ get; set; }
        public string Capacity { get; set; }
        public string EnergyClass { get; set; }
        public string Timer { get; set; }
        public string TelescopicSystem { get; set; }
        public string PyrolyticCleaning { get; set; }
        public string PushPullButtons { get; set; }
        public string Color { get; set; }
        public Technics(string title, string description, string warehouseLocation, string barCode, string Manufacturer, string Function, string Capacity, string EnergyClass, string Timer, string TelescopicSystem, string PyrolyticCleaning, string PushPullButtons, string Color) : base(title, description, warehouseLocation, barCode)
        {
        
            this.Manufacturer = Manufacturer;
            this.Function = Function;
            this.Capacity = Capacity;
            this.EnergyClass = EnergyClass;
            this.Timer = Timer;
            this.TelescopicSystem = TelescopicSystem;
            this.PyrolyticCleaning = PyrolyticCleaning;
            this.PushPullButtons = PushPullButtons;
            this.Color = Color;
        }



    }
}
