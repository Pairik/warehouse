﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseProgram
{
    class Food : Article
    {
        public DateTime ExpirationDate { get; set; }
        public string Content { get; set; }
        public string Allergens { get; set; }
        public Food(string title, string description, string warehouseLocation, string barCode, string Content, string Аllergens, DateTime ExpirationDate) : base(title, description,
            warehouseLocation, barCode)
        {
            
            this.Content = Content;
            this.Allergens = Аllergens;
            this.ExpirationDate = ExpirationDate;
        }
    }
}
