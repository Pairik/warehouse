﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseProgram
{
    abstract class Article
    {
        public Article(string title, string description, string warehouseLocation, string barCode)
        {
            this.ID = Guid.NewGuid();
            this.Title = title;
            this.Description = description;
            this.WarehouseLocation = warehouseLocation;
            this.BarCode = barCode;
        }
        
        public readonly Guid ID;
        public string Title { get; set; }
        public string Description { get; set; }
        public string CountryOfOrigin { get; set; } 
        public string Importer { get; set; }
        public string Distributor { get; set; }
        public string WarehouseLocation { get; set; }
        public string BarCode { get; set; }
        public DateTime DateImport { get; set; }
        public DateTime DateExport { get; set; }
        public DateTime DateDelete { get; set; }
        public bool Deleted { get; set; }
        public bool Scrapped { get; set; }
    }
}
